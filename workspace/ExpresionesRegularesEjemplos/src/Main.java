import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Main 
{

	public static void main(String[] args) 
	{
		String input = "(0345)423-7814";
		Pattern p = Pattern.compile("\\(011\\)[1-9]\\d{3}\\-\\d{4}|\\(0[23]4\\d\\)[1-9]\\d{2}\\-\\d{4}");
		
		Matcher m = p.matcher(input);
		
		if (m.matches())
			System.out.println("Reconocido");
		else
			System.out.println("No Reconocido");
		
		
		String input2 = "El atleta Pedro ha recibido medallas de\\n" + 
						"oro: 2545464 \\n" +
						"plata: 10 \\n" +
						"bronce: 0";
		// Cuantas medallas de oro recibio?
		
		Pattern p2 = Pattern.compile("oro\\: (\\d*)");
		
		Matcher m2 = p2.matcher(input2);
		if (m2.find())
		{
			System.out.println(m2.group());
			System.out.println(m2.group(1));
		}		
				
		String input3 = "El atleta Pedro ha recibido medallas de\\n" + 
				"oro: 2545464 \\n" +
				"plata: 10 \\n" +
				"bronce: 0";
       
		// Como se llama el atleta

		Pattern p3 = Pattern.compile("atleta ([A-Za-z]+)");

		Matcher m3 = p3.matcher(input3);
		if (m3.find())
		{
			System.out.println(m3.group());
			System.out.println(m3.group(1));
		}		
	}
}
